
execute pathogen#infect()

"if has("termguicolors")
"    set termguicolors
"else
"    set t_Co=256
"endif

set t_Co=256

set background=dark
colorscheme custom

" Go to tab by number
noremap <space>1 1gt
noremap <space>2 2gt
noremap <space>3 3gt
noremap <space>4 4gt
noremap <space>5 5gt
noremap <space>6 6gt
noremap <space>7 7gt
noremap <space>8 8gt
noremap <space>9 9gt
noremap <space>0 :tablast<cr>

set backspace=indent,eol,start
syntax on
set lazyredraw
filetype plugin on
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab
set linebreak

autocmd FileType cpp,hpp,c,h call s:c_settings()
function! s:c_settings()
    set tabstop=2
    set shiftwidth=2
endfunction


" bad spell hihglighting
setlocal spell spelllang=en_gb
set nospell

set shellslash
set grepprg=grep\ -nH\ $*
set mouse=a

" set file explorer style to be the third
let g:netrw_liststyle=3

set laststatus=2
set statusline=%f
set noshowmode
set incsearch
set hlsearch
set ignorecase
set smartcase
set autoindent
"set nonumber
set number 
set relativenumber
set nu rnu
set nocompatible
set shortmess=I " remove startup message
set shortmess+=A " disable edit notifications



au WinLeave * set norelativenumber
au FocusLost * set norelativenumber
au WinEnter * set relativenumber
au FocusGained * set relativenumber

nnoremap \j <esc>:bprev<CR>
nnoremap \k <esc>:bnext<CR>

" disable ex mode
:map Q <Nop>

"let g:indentLine_char = '�'
let g:indentLine_char = '|'
let g:indentLine_fileTypeExclude = ['tex']

" Vim
let g:indentLine_color_term = 237

" bind :Q to :q!
com -bar Q :q!
com -bar W :w

" bash auto complete
set wildmode=longest,list

" ctrl-p config
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir']
let g:ctrlp_use_caching = 0
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPBuffer'
let g:ctrlp_switch_buffer = 0

" lightline config
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }
let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
let s:palette.inactive.middle = s:palette.normal.middle
let s:palette.tabline.middle = s:palette.normal.middle

" jedi config
"let g:jedi#popup_on_dot = 0
"
"" vim latex config
"let g:tex_flavor='latex'
"let g:tex_conceal = ""
"let g:Imap_UsePlaceHolders = 0


" flake8
let g:flake8_show_in_file=1
let g:flake8_show_in_gutter=1
" let g:flake8_show_quickfix=0
"autocmd BufWritePost *.py call Flake8()



hi clear texItalStyle
hi clear texBoldStyle

tnoremap <Esc> <C-\><C-n>

" Disable character hiding, e.g. in json syntax highlighting
set conceallevel=0
let g:indentLine_setConceal = 0
let g:indentLine_fileTypeExclude = ['json']

" Black
let g:black_linelength=120

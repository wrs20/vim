
" --------------------------------
set background=dark
" - or ---------------------------
"set background=light
" --------------------------------

highlight clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="custom"


"----------------------------------------------------------------
" General settings                                              |
"----------------------------------------------------------------
"----------------------------------------------------------------
" Syntax group   | Foreground    | Background    | Style        |
"----------------------------------------------------------------

" --------------------------------
" Editor settings
" --------------------------------
hi Normal          ctermfg=none    ctermbg=none    cterm=none
hi Cursor          ctermfg=none    ctermbg=none    cterm=none
hi CursorLine      ctermfg=none    ctermbg=none    cterm=none
hi LineNr          ctermfg=grey    ctermbg=none    cterm=none
hi CursorLineNR    ctermfg=none    ctermbg=none    cterm=none

" -----------------
" - Number column -
" -----------------
hi CursorColumn    ctermfg=none    ctermbg=none    cterm=none
hi FoldColumn      ctermfg=none    ctermbg=none    cterm=none
hi SignColumn      ctermfg=none    ctermbg=none    cterm=none
hi Folded          ctermfg=none    ctermbg=none    cterm=none

" -------------------------
" - Window/Tab delimiters - 
" -------------------------
hi VertSplit       ctermfg=52      ctermbg=none    cterm=none
hi ColorColumn     ctermfg=none    ctermbg=none    cterm=none
hi TabLine         ctermfg=none    ctermbg=none    cterm=none
hi TabLineFill     ctermfg=none    ctermbg=none    cterm=none
hi TabLineSel      ctermfg=none    ctermbg=none    cterm=none

" -------------------------------
" - File Navigation / Searching -
" -------------------------------
hi Directory       ctermfg=none    ctermbg=none    cterm=none
hi Search          ctermfg=226     ctermbg=none     cterm=bold
hi IncSearch       ctermfg=none    ctermbg=none    cterm=reverse

" -----------------
" - Prompt/Status -
" -----------------
"hi StatusLine      ctermfg=none    ctermbg=none    cterm=none
"hi StatusLineNC    ctermfg=none    ctermbg=none    cterm=none
hi WildMenu        ctermfg=none    ctermbg=none    cterm=none
hi Question        ctermfg=none    ctermbg=none    cterm=none
hi Title           ctermfg=none    ctermbg=none    cterm=none
hi ModeMsg         ctermfg=none    ctermbg=none    cterm=none
hi MoreMsg         ctermfg=none    ctermbg=none    cterm=none

" --------------
" - Visual aid -
" --------------
hi MatchParen      ctermfg=none    ctermbg=none    cterm=bold
hi Visual          ctermfg=none    ctermbg=none    cterm=reverse
hi VisualNOS       ctermfg=none    ctermbg=none    cterm=none
hi NonText         ctermfg=none    ctermbg=none    cterm=none

hi Todo            ctermfg=196     ctermbg=none    cterm=none
hi Underlined      ctermfg=none    ctermbg=none    cterm=none
hi Error           ctermfg=none    ctermbg=none    cterm=none
hi ErrorMsg        ctermfg=none    ctermbg=none    cterm=none
hi WarningMsg      ctermfg=none    ctermbg=none    cterm=none
hi Ignore          ctermfg=none    ctermbg=none    cterm=none
hi SpecialKey      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Variable types
" --------------------------------
hi Constant        ctermfg=087     ctermbg=none    cterm=none
hi String          ctermfg=087     ctermbg=none    cterm=none
hi StringDelimiter ctermfg=087     ctermbg=none    cterm=none
hi Character       ctermfg=087     ctermbg=none    cterm=none
hi Number          ctermfg=087     ctermbg=none    cterm=none
hi Boolean         ctermfg=087     ctermbg=none    cterm=none
hi Float           ctermfg=087     ctermbg=none    cterm=none

hi Identifier      ctermfg=075     ctermbg=none    cterm=none
hi Function        ctermfg=075     ctermbg=none    cterm=none

" --------------------------------
" Language constructs
" --------------------------------
hi Statement       ctermfg=141     ctermbg=none    cterm=none
hi Conditional     ctermfg=141     ctermbg=none    cterm=none
hi Repeat          ctermfg=141     ctermbg=none    cterm=none
hi Label           ctermfg=141     ctermbg=none    cterm=none
hi Operator        ctermfg=141     ctermbg=none    cterm=none
hi Keyword         ctermfg=141     ctermbg=none    cterm=none
hi Exception       ctermfg=141     ctermbg=none    cterm=none
"hi Comment         ctermfg=8       ctermbg=none    cterm=none
hi Comment         ctermfg=11       ctermbg=none    cterm=none

hi Special         ctermfg=215     ctermbg=none    cterm=none
hi SpecialChar     ctermfg=none    ctermbg=none    cterm=none
hi Tag             ctermfg=none    ctermbg=none    cterm=none
hi Delimiter       ctermfg=none    ctermbg=none    cterm=none
hi SpecialComment  ctermfg=none    ctermbg=none    cterm=none
hi Debug           ctermfg=none    ctermbg=none    cterm=none

" ----------
" - C like -
" ----------
hi PreProc         ctermfg=034     ctermbg=none    cterm=none
hi Include         ctermfg=034     ctermbg=none    cterm=none
hi Define          ctermfg=034     ctermbg=none    cterm=none
hi Macro           ctermfg=034     ctermbg=none    cterm=none
hi PreCondit       ctermfg=034     ctermbg=none    cterm=none

hi Type            ctermfg=121     ctermbg=none    cterm=none
hi StorageClass    ctermfg=121     ctermbg=none    cterm=none
hi Structure       ctermfg=121     ctermbg=none    cterm=none
hi Typedef         ctermfg=121     ctermbg=none    cterm=none

" --------------------------------
" Diff
" --------------------------------
hi DiffAdd         ctermfg=Black   ctermbg=LightGreen  cterm=none
hi DiffChange      ctermfg=Black   ctermbg=LightYellow cterm=none
hi DiffDelete      ctermfg=Black   ctermbg=LightRed    cterm=none
hi DiffText        ctermfg=Black   ctermbg=LightCyan   cterm=none


" --------------------------------
" Completion menu
" --------------------------------
hi Pmenu           ctermfg=none    ctermbg=53      cterm=none
hi PmenuSel        ctermfg=none    ctermbg=19      cterm=none
hi PmenuSbar       ctermfg=none    ctermbg=53      cterm=none
hi PmenuThumb      ctermfg=none    ctermbg=53      cterm=none

" --------------------------------
" Spelling
" --------------------------------
hi SpellBad        ctermfg=none    ctermbg=88      cterm=none
hi SpellCap        ctermfg=none    ctermbg=none    cterm=none
hi SpellLocal      ctermfg=none    ctermbg=none    cterm=none
hi SpellRare       ctermfg=none    ctermbg=none    cterm=none

"--------------------------------------------------------------------
" Specific settings                                                 |
"--------------------------------------------------------------------
